/* eslint-disable no-console */
const WebSocket = require("ws");
const fs = require("fs");
const debug = require("debug")("index");
const spamDebug = require("debug")("spam");
const DiscordAPI = require("./src/types/API");
const payloads = require("./src/types/payloads");
const config = require("./config");

const processData = require("./src/processData");

const api = new DiscordAPI(config.token);
const ws = new WebSocket("wss://gateway.discord.gg/?v=10&encoding=json");

require("./src/embedServer").init();

const presences = ["online", "idle", "dnd"];
let i = 0;
// eslint-disable-next-line no-return-assign
const getPresence = () => presences[i += 1] || presences[i = 0];
// const ops = {
//   0: "DATA",
//   1: "HEARTBEAT",
//   2: "IDENTIFY",
//   3: "UPDATE_PRESENCE",
//   4: "UPDATE_VOICE_STATE",
//   6: "RESUME",
//   8: "REQUEST_GUILD_MEMBERS",
//   11: "HEARTBEAT_ACK",
//   10: "HELLO",
// };
// const iops = Object
//   .entries(ops)
//   .reduce((all, [key, value]) => ({ ...all, [value]: parseInt(key, 10) }), {});

ws.on("open", () => {
  processData.init();
  debug("websocket opened");
  const payload = payloads.Identify(config.token);
  ws.send(payload);

  setTimeout(() => {
    setInterval(() => {
      const conf = JSON.parse(fs.readFileSync("./temp-config.json", "utf8"));
      if (!conf.scrambler) return;
      const p = getPresence();
      const data = payloads.UpdatePresence(p);
      ws.send(data);
      console.log(p);
    }, 750);
  }, 2000);
});

ws.on("message", (data) => {
  let json;
  try {
    json = JSON.parse(data.toString());
  } catch (e) {
    console.log("payload parsing error", e, data.toString());
  }

  // spamDebug("jsonData", json);
  // console.log(json);

  switch (json.op) {
    case 0: // data
      processData.run(json, api);
      break;
    case 10:
      debug("> HELLO", `heartbeat_interval=${json.d.heartbeat_interval}`);
      setInterval(() => {
        const payload = payloads.Heartbeat();
        ws.send(payload);
        spamDebug("< HEARTBEAT");
      }, json.d.heartbeat_interval);
      break;
    default:
      debug(`unrecognized operation: ${json.op}`);
      break;
  }
});

ws.on("close", process.exit);
