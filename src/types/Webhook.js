const axios = require("axios");

class Webhook {
  constructor(url) {
    this.url = url;
    this.axios = axios.create({
      baseURL: "https://discord.com/api/v9", // /api/webhooks ?
    });
  }

  sendMessage(data) {
    return this.axios.post(this.url, data);
  }
}

module.exports = Webhook;
