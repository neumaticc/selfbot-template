/* eslint-disable max-classes-per-file */
class BaseMessage {
  constructor(content) {
    this.content = content;
  }
}

const botTag = (author) => {
  if (author.bot && author.discriminator === "0000") return "[WEBHOOK]";
  if (author.bot) return "[WEBHOOK]";
  return "";
};

class Ghostping extends BaseMessage {
  constructor(data) {
    super(undefined);
    this.embeds = [{
      title: "Ghost pinged",
      author: {
        icon_url: data.author?.avatar ? `https://cdn.discordapp.com/avatars/${data.author.id}/${data.author.avatar}.png` : "https://cdn.discordapp.com/embed/avatars/0.png",
        name: `${botTag(data.author)}${data.author.username}#${data.author.discriminator}`,
      },
      description: `${data.author.username}#${data.author.discriminator} (\`${data.author.id}\`) ghost-pinged in [${data.channel.name}](https://discord.com/channels/${data.guild.id}/${data.channel.id})`
      + `\n\`Guild:\` ${data.guild.name} (${data.guild.id})`
      + `\n\`Channel:\` #${data.channel.name} (${data.channel.id})`
      + `\n\`@everyone:\` ${data.mention_everyone}`,
    }];
  }
}

/*
{
    item: "",
    author: "",
    ends: 0,
    service: {
      avatar: "",
      name: ""
    },
    channel: cached channel,
  }
*/
class GiveawayEntry extends BaseMessage {
  constructor({
    item,
    author,
    ending,
    service,
    channel,
  }) {
    super(undefined);
    this.embeds = [{
      title: "Giveaway entry",
      author: {
        icon_url: service.avatar,
        name: service.name,
      },
      description: `Entered giveaway for \`${item}\` in [\`#${channel.name}\`](https://discord.com/channels/${channel.guild_id}/${channel.id})`
      + `\n\nAuthor: \`${author}\``
      + `\nEnds: ${!ending.timestamp ? ending.data : "unknown"}`, // make into a discord timestamp - <t:{}:R>
    }];
  }
}

class FriendAdded extends BaseMessage {
  constructor(data, isIncoming) {
    super(undefined);
    this.embeds = [{
      title: "Friend added",
      author: {
        icon_url: data.user.avatar ? `https://cdn.discordapp.com/avatars/${data.user.id}/${data.user.avatar}` : "https://cdn.discordapp.com/embed/avatars/0.png",
        name: `${data.user.username}#${data.user.discriminator} (${data.user.id})`,
      },
      description: isIncoming ? `\`${data.user.username}#${data.user.discriminator}\` sent a friend request` : `A friend request was sent to \`${data.user.username}#${data.user.discriminator}\``,
    }];
  }
}

class FriendRemoved extends BaseMessage {
  constructor(data, isIncoming) {
    super(undefined);
    this.embeds = [{
      title: "Friend added",
      author: {
        icon_url: data.user.avatar ? `https://cdn.discordapp.com/avatars/${data.user.id}/${data.user.avatar}` : "https://cdn.discordapp.com/embed/avatars/0.png",
        name: `${data.user.username}#${data.user.discriminator} (${data.user.id})`,
      },
      description: isIncoming ? `\`${data.user.username}#${data.user.discriminator}\` sent a friend request` : `A friend request was sent to \`${data.user.username}#${data.user.discriminator}\``,
    }];
  }
}

module.exports = {
  BaseMessage,
  Ghostping,
  GiveawayEntry,
  FriendAdded,
  FriendRemoved,
};
