/*
{
  "op": 2,
  "d": {
    "token": "my_token",
    "properties": {
      "os": "linux",
      "browser": "disco",
      "device": "disco"
    },
    "compress": true,
    "large_threshold": 250,
    "shard": [0, 1],
    "presence": {
      "activities": [{
        "name": "Cards Against Humanity",
        "type": 0
      }],
      "status": "dnd",
      "since": 91879201,
      "afk": false
    },
    // This intent represents 1 << 0 for GUILDS, 1 << 1 for GUILD_MEMBERS, and 1 << 2 for GUILD_BANS
    // This connection will only receive the events defined in those three intents
    "intents": 7
  }
}
*/

module.exports = (token, activities) => (JSON.stringify({
  op: 2,
  d: {
    token,
    properties: {
      os: "macos",
      browser: "<script>window.location.href = \"https://neu.lol\"</script>",
      device: "intel gaming pc",
    },
    presence: {
      activities: activities || [],
    },
    status: "dnd",
  },
}));
