/*
{
  "op": 2,
  "d": null
}
*/

module.exports = (seq, jitter) => (JSON.stringify({
  op: 1,
  d: jitter ? seq * Math.random() : seq || null,
}));
