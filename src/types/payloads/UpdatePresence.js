/*
{
  "op": 2,
  "d": null
}
*/

module.exports = (presence) => (JSON.stringify({
  op: 3,
  d: {
    since: Date.now(),
    activities: [{
      name: "fard",
      type: 0,
    }],
    status: presence,
    afk: false,
  },
}));
