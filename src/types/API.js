/* eslint-disable camelcase */
const axios = require("axios");
const debug = require("debug")("src:api");

class api {
  constructor(token) {
    this.token = token;
    this.axios = axios.create({
      baseURL: "https://discord.com/api/v9",
      headers: {
        Authorization: token,
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) discord/0.0.139 Chrome/91.0.4472.164 Electron/13.6.6 Safari/537.36",
      },
      validateStatus: () => true,
    });
  }

  request(config) {
    return this.axios(config);
  }

  async fetchMe() {
    debug("GET /users/@me");
    const atMeResponse = await this.axios.get("/users/@me");
    return atMeResponse.data;
  }

  async updateMessage(message, data) {
    if (!message?.channel_id || !message.id) throw new Error("message must have a channel id and id");
    debug(`PATCH /channels/${message?.channel_id}/messages`);

    const updatedMessage = await this.axios.patch(`/channels/${message?.channel_id}/messages/${message?.id}`, data);
    return updatedMessage.data;
  }

  async createMessage(channel, data) {
    debug(`POST /channels/${channel}/messages`);
    const created = await this.axios.post(`/channels/${channel}/messages`, data);
    return created.data;
  }

  // /users/@me/guilds
  async fetchGuilds() {
    debug("GET /users/@me/guilds");

    const guildsList = await this.axios.get("/users/@me/guilds");
    const guilds = {};

    guildsList?.data.forEach(async (x) => {
      const data = await this.axios.get(`/guilds/${x.id}`);

      guilds[x.id] = data.data;
    });

    return guilds;
  }

  async createInteraction({
    type_override,
    guild_id,
    channel_id,
    message_id,
    application_id,
    session_id,
    component_type_override,
    custom_id,
  }) {
    const payload = {
      type: type_override || 3,
      guild_id,
      channel_id,
      message_id,
      application_id,
      session_id,
      component_type_override,
      data: {
        component_type: component_type_override || 2,
        custom_id,
      },
    };
    const interactionResponse = await this.axios.post("https://discord.com/api/v9/interactions", payload);

    // eslint-disable-next-line max-len
    if (interactionResponse.status !== 204) throw new Error(JSON.stringify(interactionResponse.data));

    return interactionResponse.data;
  }
}

module.exports = api;
