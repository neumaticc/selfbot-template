/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
const fs = require("fs");
const debug = require("debug")("src:processData");
const Events = require("events");

const ee = new Events();

const run = async (data) => {
  debug(`[(${process.uptime()}s) ${new Date().toLocaleString()}] event`, data.t);
  ee.emit(data.t, data);
};

const init = () => {
  fs.readdirSync("./src/events").forEach((x) => {
    // eslint-disable-next-line import/no-dynamic-require, global-require
    const imp = require(`./events/${x}`);
    ee.on(imp.name, imp.run);
  });

  // eslint-disable-next-line global-require
  require("./commandHandler").init(ee);
};

module.exports = {
  run,
  init,
};
