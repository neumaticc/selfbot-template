/* eslint-disable no-console */
const fs = require("fs");
const debug = require("debug")("src:cache");
const config = require("../config");
const DiscordAPI = require("./types/API");

const api = new DiscordAPI(config.token);
const recent = new Map();

const getChannel = async (id) => {
  // channel is in recent cache list
  if (recent.get(id)) {
    debug(`[channel] CACHE HIT [recent] <${id}>`);

    return recent.get(id);
  }

  if (fs.existsSync(`./cache/channels/${id}.json`)) {
    debug(`[channel] CACHE HIT [file] <${id}>`);
    // channel is in cache, but not in the recent cache list
    const data = JSON.parse(fs.readFileSync(`./cache/channels/${id}.json`));

    recent.set(data.id, data);
    setTimeout(() => { recent.delete(data.id); }, 60000);
    return data;
  }

  debug(`[channel] CACHE MISS <${id}>`);
  // channel is not cached
  const data = await api.axios.get(`/channels/${id}`);
  if (data.status !== 200) return console.error((new Error(`api error ${JSON.stringify(data.data)}`)).stack);

  fs.writeFileSync(`./cache/channels/${id}.json`, JSON.stringify({ ...data.data, exp: Date.now() + 60000 * 60 * 48 })); // 48h
  recent.set(data.id, data);
  setTimeout(() => { recent.delete(data.id); }, 60000);

  return data.data;
};

const getGuild = async (id) => {
  // guild is in recent cache list
  if (recent.get(id)) {
    debug(`CACHE HIT [recent] <${id}>`);
    return recent.get(id);
  }

  if (fs.existsSync(`./cache/guilds/${id}.json`)) {
    debug(`[guild] CACHE HIT [file] <${id}>`);
    // guild is in cache, but not in the recent cache list
    const data = JSON.parse(fs.readFileSync(`./cache/guilds/${id}.json`));

    recent.set(data.id, data);
    setTimeout(() => { recent.delete(data.id); }, 60000);
    return data;
  }

  debug(`[guild] CACHE MISS <${id}>`);
  // guild is not cached
  const data = await api.axios.get(`/guilds/${id}`);
  const channelData = await api.axios.get(`/guilds/${id}/channels`);

  if (data.status !== 200) console.error((new Error(`api error ${JSON.stringify(channelData.data)}`)).stack);
  if (channelData.status !== 200) console.error((new Error(`api error ${JSON.stringify(channelData.data)}`)).stack);

  const finalData = {
    ...data.data,
    channels: channelData.data,
  };
  fs.writeFileSync(`./cache/guilds/${id}.json`, JSON.stringify({ ...finalData, exp: Date.now() + 60000 * 60 * 48 })); // 48h
  recent.set(finalData.id, finalData);
  setTimeout(() => { recent.delete(finalData.id); }, 60000);

  return finalData;
};

module.exports = {
  getChannel,
  getGuild,
};
