const crypto = require("crypto");
const debug = require("debug")("utils:taskManager");

// repeating tasks
const tasks = new Map();
const taskIntervals = new Map();

const init = () => {
  setInterval(() => {
    tasks.forEach((id, data) => {
      if (data.iterations === data.iterationsRan) {
        clearInterval(taskIntervals.get(id));

        debug(`removing task ${id}`);
        tasks.delete(id);
        taskIntervals.delete(id);
      }
    });

    taskIntervals.forEach((id, runner) => {
      if (!tasks.get(id)) {
        debug(`removing runner ${id}: orphan runner found`);
        clearInterval(runner);
        taskIntervals.delete(id);
      }
    });
  });
};

/*
    {
        "id": "",
        "description": "",
        "run": "",
        "interval": "",
        "iterations": 0
        "iterationsRan": 0
    }
*/
const create = (fn, {
  description,
  interval,
  iterations,
}) => {
  const ID = crypto.randomUUID();

  tasks.set(ID, {
    id: ID,
    description,
    run: fn,
    interval,
    iterations,
    iterationsRan: 0,
  });

  const runner = setInterval(async () => {
    const data = tasks.get(ID);
    if (!data) {
      debug(`[${ID}] [runner] detected orphan task, killing`);
      clearInterval(this);
    }
    tasks.set(data, { ...data, iterationsRan: data.iterationsRan + 1 });

    if (data.iterationsRan >= data.iterations && data.iterations !== false) {
      debug(`[${ID}] [runner] reached max iterations, killing`);
      clearInterval(this);
    }

    await fn();
  }, interval);
  taskIntervals.set(ID, runner);

  return ID;
};

const kill = (id) => {
  const task = tasks.get(id);
  const runner = taskIntervals.get(id);

  if (!task || !runner) return { success: false, error: "no task with specific id found" };

  clearInterval(runner);
  return { success: true };
};

module.exports = {
  init,
  kill,
  create,
  tasks,
};
