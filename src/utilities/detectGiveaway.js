const debug = require("debug")("utils:detectGiveaway");
const handlers = require("./giveaways");
const Alerts = require("../types/Alerts");
const webhooks = require("../webhooks");

/*
{
    item: "",
    author: "",
    ending: {
        timestamp: false
        data: ""
    }
    service: {
      avatar: "",
      name: ""
    },
    channel: cached channel,
  }
*/
// eslint-disable-next-line consistent-return
const detectGiveaway = async (message) => {
  let sendAlert = false;
  let alertData = {};
  // dyno
  if (message.author.username === "Dyno") {
    if (!message.embeds.length) return debug("dyno: no embeds");

    const actionRow = message?.components[0];
    const comp = actionRow?.components?.find((x) => x.label === "Enter");
    if (!comp) return debug("dyno: no button");

    debug("real dyno giveaway");
    alertData = await handlers.dyno(message);
    sendAlert = true;
  }

  // dasu
  if (message.author.id === "949305358520844288") {
    const embed = message.embeds[0];

    if (!embed
      || embed.url !== "https://dasu.gg"
      || embed.thumbnail.url !== "https://dasu.gg/images/tada.gif") return false;

    debug("real dasu/kori giveaway");
    alertData = await handlers.dasu(message);
    sendAlert = true;
  }

  // giveawaybot
  if (message.author.id === "294882584201003009") {
    const embed = message.embeds[0];

    if (!embed || !embed.description.includes("Entries:")) return false;

    debug("real giveawaybot giveaway");
    alertData = await handlers.giveawaybot(message);
    sendAlert = true;
  }

  // giveaway boat
  if (message.author.id === "530082442967646230") {
    const embed = message.embeds[0];

    if (!embed || !embed.description.split("\n")[0].match(/React with 🎉(.*) to enter!/g)) return false;

    debug("real giveaway boat giveaway");
    alertData = await handlers.giveawayboat(message);
    sendAlert = true;
  }

  if (sendAlert) {
    debug(JSON.stringify(alertData, null, 2))
    const payload = new Alerts.GiveawayEntry(alertData);
    await webhooks.alerts_giveaways.sendMessage(payload);
  }
};

module.exports = detectGiveaway;
