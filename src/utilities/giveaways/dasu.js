/* eslint-disable camelcase */
const debug = require("debug")("utils:giveaways:dasu");
const config = require("../../../config");
const DiscordAPI = require("../../types/API");
const user = require("../../../cache/user.json");
const { getChannel } = require("../../cache");

const api = new DiscordAPI(config.token);

const run = async (message) => {
  const actionRow = message?.components[0];
  const enterButton = actionRow?.components?.find((x) => x.label === "Join Giveaway");
  if (!enterButton) return debug("No enterButton component found for", message.id);

  await api.createInteraction({
    guild_id: message.guild_id,
    channel_id: message.channel_id,
    message_id: message.id,
    application_id: "949305358520844288",
    session_id: user.session_id,
    custom_id: enterButton.custom_id,
  });

  const embed = message.embeds[0];
  const item = embed.title;
  const author = embed.footer.text.split("Hosted By: ")[1];
  const endingData = embed.description.split("Ends ")[1];
  const channel = await getChannel(message.channel_id);

  return {
    item,
    author,
    ending: {
      timestamp: false,
      data: endingData,
    },
    service: {
      avatar: "https://cdn.discordapp.com/avatars/949305358520844288/dd8383eb0ed9718342c763654f203bbc.png",
      name: "Dasu/Kori",
    },
    channel,
  };
};

module.exports = run;
