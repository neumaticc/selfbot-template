/* eslint-disable camelcase */
const debug = require("debug")("utils:giveaways:giveawaybot");
const config = require("../../../config");
const DiscordAPI = require("../../types/API");
const user = require("../../../cache/user.json");
const { getChannel } = require("../../cache");

const api = new DiscordAPI(config.token);

const run = async (message) => {
  const actionRow = message?.components[0];
  const enterButton = actionRow?.components?.find((x) => x.custom_id === "enter-giveaway");
  if (!enterButton) return debug("No enterButton component found for", message.id);

  await api.createInteraction({
    guild_id: message.guild_id,
    channel_id: message.channel_id,
    message_id: message.id,
    application_id: "294882584201003009",
    session_id: user.session_id,
    custom_id: enterButton.custom_id,
  });

  const embed = message.embeds[0];
  const data1 = (embed.description.includes("\n\n\n") ? embed.description.split("\n\n\n")[1] : embed.description).split("\n").map((x) => {
    const [key, value] = x.split(": ");
    return [key.toLowerCase(), value];
  });
  const data = {};
  // eslint-disable-next-line no-return-assign
  data1.forEach(([x], i) => data[x] = data1[i]);
  const item = embed.title;
  let author = (data["hosted by"][1]).replace(/<(.*)>/g, "$1");
  const endingData = data.ends[1];
  const channel = await getChannel(message.channel_id);

  const userData = await api.axios.get(`/users/${author}/profile`);
  author = userData?.data?.username || "Unknown";

  return {
    item,
    author,
    ending: {
      timestamp: false,
      data: endingData,
    },
    service: {
      avatar: "https://cdn.discordapp.com/avatars/294882584201003009/4bfd18c35f26a86f2b5b4eb2ad81a1f7.png",
      name: "GiveawayBot",
    },
    channel,
  };
};

module.exports = run;
