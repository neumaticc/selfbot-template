/* eslint-disable camelcase */
const config = require("../../../config");
const DiscordAPI = require("../../types/API");
const { getChannel } = require("../../cache");

const api = new DiscordAPI(config.token);

const run = async (message) => {
  await api.axios.put(`/channels/${message.channel_id}/messages/${message.id}/reactions/%F0%9F%8E%89/%40me?location=Message&burst=false`, {});
  const embed = message.embeds[0];
  const item = embed.title;
  const author = "[Not available with Giveaway Boat]";
  const ts = new Date(embed.timestamp).getTime;
  const endingData = `<t:${ts.toString().substr(0, ts.toString().length - 3)}:R>`; // ${subTime.toString().substr(0, subTime.toString().length - 3)}
  const channel = await getChannel(message.channel_id);

  return {
    item,
    author,
    ending: {
      timestamp: false,
      data: endingData,
    },
    service: {
      avatar: "https://cdn.discordapp.com/avatars/530082442967646230/d08b578ba7011e73b296efd46ea971f0.png",
      name: "Giveaway Boat",
    },
    channel,
  };
};

module.exports = run;
