/* eslint-disable camelcase */
const debug = require("debug")("utils:giveaways:dyno");
const config = require("../../../config");
const DiscordAPI = require("../../types/API");
const user = require("../../../cache/user.json");
const { getChannel } = require("../../cache");

const api = new DiscordAPI(config.token);

const run = async (message) => {
  const actionRow = message?.components[0];
  const enterButton = actionRow?.components?.find((x) => x.label === "Enter");
  if (!enterButton) return debug("No enterButton component found for", message.id);

  await api.createInteraction({
    guild_id: message.guild_id,
    channel_id: message.channel_id,
    message_id: message.id,
    application_id: "155149108183695360",
    session_id: user.session_id,
    custom_id: enterButton.custom_id,
  });

  const embed = message.embeds[0];
  const item = embed.title;
  const author = embed.fields.find((x) => x.name === "Hosted By")?.value?.replace(/<@(.*)>/g, "$1");
  const endingData = `<${embed.fields.find((x) => x.name === "Time Remaining")?.value?.replace(/\*\*<(.*)>\*\* \(\[Timer\](.*)/g, "$1")}>`;
  const channel = await getChannel(message.channel_id);

  return {
    item,
    author,
    ending: {
      timestamp: false,
      data: endingData,
    },
    service: {
      avatar: "https://cdn.discordapp.com/avatars/155149108183695360/19a5ee4114b47195fcecc6646f2380b1.png",
      name: "Dyno",
    },
    channel,
  };
};

module.exports = run;
