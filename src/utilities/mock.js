module.exports = (input) => {
  const upperCase = String().toUpperCase;
  const lowerCase = String().toLowerCase;
  const chars = [...input];
  return chars.map((value, index) => (index % 2 ? lowerCase.apply(value) : upperCase.apply(value))).join("");
};
