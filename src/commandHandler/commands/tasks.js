/* eslint-disable consistent-return */
const casual = require("casual");

const run = async (message) => {
  if (!message.args.length) return message.api.updateMessage(":x: No user provided to dox!");

  const user = Number.isNaN(parseInt(message.args[0], 10)) ? message.args[0].replace(/<@!?(.*)>/g, "$1") : message.args[0];
  if (Number.isNaN(parseInt(user, 10))) return message.api.updateMessage(":x: No user provided to dox!");

  const nt = Math.round(Math.random() * 10);
  await message.api.updateMessage(message, { content: `:yellow_circle: Searching all database breaches for <@${user}>...` });
  setTimeout(async () => {
    await message.api.updateMessage(message, { content: `:yellow_circle: Looking up address of <@${user}>...` });
  }, 500);
  setTimeout(async () => {
    await message.api.updateMessage(message, { content: `:yellow_circle: Looking known IPs of <@${user}>...` });
  }, 1500);
  setTimeout(async () => {
    await message.api.updateMessage(message, { content: `:yellow_circle: Finding public records for <@${user}>...` });
  }, 2000);
  setTimeout(async () => {
    await message.api.updateMessage(message, { content: `:green_circle: <@${user}>'s real name is \`${casual.full_name}\`, their address is \`${casual.country}, ${casual.address}\`, and the password they used \`${nt}\` time${nt === 0 ? "" : "s"} across the internet is \`${casual.password}\`` });
  }, 3000);
};

module.exports = {
  name: "dox",
  run,
};
