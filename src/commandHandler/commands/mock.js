/* eslint-disable consistent-return */
const mock = require("../../utilities/mock");

const run = async (message) => {
  const data = message.args.join(" ");

  await message.api.updateMessage(message, { content: mock(data) });
};

module.exports = {
  name: "mock",
  run,
};
