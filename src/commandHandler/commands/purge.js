/* eslint-disable consistent-return */
const config = require("../../../config");

let ratelimitUntil = 0;

const nextSteps = async (message, messages) => {
  const ms = messages;
  setInterval(async () => {
    if (ms.length === 0) return clearInterval(this);
    console.log(ms.length);
    if (ratelimitUntil >= Date.now()) return console.log("ratelimited for", ratelimitUntil - Date.now(), "ms");

    const id = ms.shift();

    console.log("delete", id);
    const resp = await message.api.axios.delete(`/channels/${message.channel_id}/messages/${id}`);
    if (resp.status !== 200) console.log(JSON.stringify(resp.data, null, 2));
    if (resp?.data?.retry_after) {
      ms.push(id);
      ratelimitUntil = Date.now() + resp.data.retry_after * 1000 + 1000;
    }
  }, 100);
};

const run = async (message) => {
  const amt = parseInt(message.args[0], 10) || 50;
  const filter = message.args[1];
  const filterQuery = message.args[2];
  await message.api.updateMessage(message, { content: `:arrow_forward: fetching \`${amt}\` messages${filter ? ` with filter ${filter}=${filterQuery}` : ""}` });

  const messages = [];
  let amount = Number.isNaN(amt) ? 50 : amt;
  let lastId = null;
  let inProgress = false;
  let tries = 0;

  setInterval(async () => {
    if (messages.length === amount || tries >= 10) {
      clearInterval(this);
      nextSteps(message, messages);
      return;
    }
    if (inProgress) return console.log("another process in progress");
    inProgress = true;
    tries += 1;

    if (amount === 0) {
      tries = 10;
      return;
    }

    const data = await message.api.axios.get(`/channels/${message.channel_id}/messages?limit=100${lastId ? `&before=${lastId}` : ""}`);
    if (data?.data?.length < 100) tries = 10;

    if (!data?.data?.length) {
      tries = 10;
      return;
    }
    const msgs = data.data.filter((y) => y.author.id === config.id);

    let aa = 0;
    msgs.forEach((msg) => {
      if (aa >= amount) return console.log("skip", msg.id);
      aa += 1;
      messages.push(msg.id);
    });

    lastId = msgs[msgs.length - 1];
    amount -= msgs.length;
    inProgress = false;
  }, 500);
};

module.exports = {
  name: "purge",
  run,
};
