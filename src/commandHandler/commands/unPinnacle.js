/* eslint-disable no-console */
/* eslint-disable consistent-return */
let ratelimitUntil = 0;

const run = async (message) => {
  const messages = [];
  const data = await message.api.axios.get(`/channels/${message.channel_id}/pins`);

console.log(data.data)
  data.data.forEach((x) => {
    messages.push(x.id);
  });

  setInterval(async () => {
    if (ratelimitUntil >= Date.now()) return;

    const id = messages.shift();
    if (!id) return clearInterval(this);

    const d = await message.api.axios.delete(`/channels/${message.channel_id}/pins/${id}`);

    // if (d.status !== 200) console.log(JSON.stringify(d.data, null, 2));
    if (d?.data?.retry_after) {
      messages.push(id);
      ratelimitUntil = Date.now() + d.data.retry_after * 1000 + 1000;
    } else {
      console.log(`Removed pinned message: ${id}`);
    }
  });

  let sent = false;
  setInterval(async () => {
    if (!messages.length) {
      clearInterval(this);
      if (sent) return;
      sent = true;
      await message.api.updateMessage(message, {
        content: ":white_check_mark: done",
      });
    }
  });
};

module.exports = {
  name: "unpinnacle",
  run,
};
