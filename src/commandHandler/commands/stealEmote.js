/* eslint-disable consistent-return */
const axios = require("axios");

const confirmations = new Map();

const run = async (message) => {
  if (!message.args.length) return message.api.updateMessage(":x: No emote provided!");

  if (message.args[0] === "confirm") {
    if (!confirmations.get(message.args[1])) return message.api.updateMessage(":x: Invalid confirmation token");
    const data = confirmations.get(message.args[1]);

    const emote = await axios.get(`https://cdn.discordapp.com/emojis/${data.id}${data.animated ? ".gif" : ".png"}`, {
      responseType: "arraybuffer",
    });
    const b64 = `data:image/${data.animated ? "gif" : "png"};base64,${Buffer.from(emote.data).toString("base64")}`;

    const emoteCreate = await message.api.axios.post(`/guilds/${message.args[2]}/emojis`, {
      image: b64,
      name: data.name,
    });

    if (emoteCreate.status !== 201) {
      return message.api.updateMessage(message, { content: `:x: DiscordAPIError${emoteCreate.data?.code ? ` \`${emoteCreate.data?.code}\`` : ""}\n\`\`\`json\n${JSON.stringify(emoteCreate.data)}\n\`\`\`` });
    }

    confirmations.delete(message.args[1]);
    return message.api.updateMessage(message, { content: `:white_check_mark: Created <${data.animated ? "a" : ""}:${data.name}:${emoteCreate.data.id}>` });
  }

  const [animated, name, id] = message.args[0].replace(/<(a)?:(.*):(.*)>/g, "$1,$2,$3").split(",");

  if (!name || !id) return message.api.updateMessage(":x: No emote provided!");

  const confirmId = Math.random().toString(36).substr(2);

  confirmations.set(confirmId, { name, id, animated: animated === "a" });

  await message.api.updateMessage(message, { content: `<:${name}:${id}> confirmId: ${confirmId}\n\`\`\`..se confirm ${confirmId} [guildId]\`\`\`` }); // \nguilds:\`\`\`yaml\n${guilds}\n\`\`\`
};

module.exports = {
  name: "stealEmote",
  aliases: ["se"],
  run,
};
