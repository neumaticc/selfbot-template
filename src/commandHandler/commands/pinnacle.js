/* eslint-disable consistent-return */
let ratelimitUntil = 0;

const run = async (message) => {
  const messages = [];
  const data = await message.api.axios.get(`/channels/${message.channel_id}/messages?limit=50`);

  data.data.forEach((x) => {
    messages.push(x.id);
  });
  let done1 = 0;

  setInterval(async () => {
    if (done1 === messages.length) return clearInterval(this);
    done1 += 1;

    if (ratelimitUntil >= Date.now()) return;

    const id = messages.shift();
    if (!id) return clearInterval(this);

    const d = await message.api.axios.put(`/channels/${message.channel_id}/pins/${id}`);

    // if (d.status !== 200) console.log(JSON.stringify(d.data, null, 2));
    if (d?.data?.retry_after) {
      messages.push(id);
      ratelimitUntil = Date.now() + d.data.retry_after * 1000 + 3000;
    }
  });
};

module.exports = {
  name: "pinnacle",
  run,
};
