/* eslint-disable consistent-return */
const { embeds } = require("../../embedServer");

const invis = require("../../utilities/invis.json").text;

const run = async (message) => {
  const data = message.args.join(" ");
  const args = data.split(/\n/g).filter((x) => !!x);

  if (!args.length) return message.api.updateMessage(message, { content: ":x: 1 argument is required" });

  const id = Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2);
  embeds.set(id, {
    title: args[0],
    color: args[1],
    description: args[1],
  });

  await message.api.updateMessage(message, { content: `${invis} https://amogus.loca.lt/embed?id=${id}` });
};

module.exports = {
  name: "embed",
  run,
};
