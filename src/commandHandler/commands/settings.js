/* eslint-disable consistent-return */
const fs = require("fs");

const run = async (message) => {
  const conf = JSON.parse(fs.readFileSync("./temp-config.json"));

  if (!message.args[0]) {
    return message.api.updateMessage(message, { content: `Current settings:\n\`\`\`yaml\nscrambler: ${conf.scrambler}\n\`\`\`` });
  }

  switch (message.args[0]) {
    case "sc" || "scrambler":
      if (["on", "true", "1"].includes(message.args[1])) conf.scrambler = true;
      else if (["off", "false", "0"].includes(message.args[1])) conf.scrambler = false;

      fs.writeFileSync("./temp-config.json", JSON.stringify(conf, null, 2));
      return message.api.updateMessage(message, { content: `:green_circle: Set \`scrambler\` to \`${conf.scrambler}\`` });
    default:
      return message.api.updateMessage(message, { content: ":x: Invalid option" });
  }
};

module.exports = {
  name: "settings",
  aliases: ["s"],
  run,
};
