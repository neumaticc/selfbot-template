/* eslint-disable consistent-return */
const fs = require("fs");
const debug = require("debug")("src:commandHandler");
const config = require("../../config");
const DiscordAPI = require("../types/API");

const api = new DiscordAPI(config.token);
const commands = new Map();

module.exports.init = (ee) => {
  fs.readdirSync("./src/commandHandler/commands").forEach((x) => {
    // eslint-disable-next-line import/no-dynamic-require, global-require
    const data = require(`./commands/${x}`);
    commands.set(data.name, data);

    if (data.aliases) {
      data.aliases.forEach((y) => {
        commands.set(y, data);
      });
    }
  });

  ee.on("MESSAGE_CREATE", async (data) => {
    // console.log(api);
    const message = {
      ...data.d,
      api,
    //   reply: api.respondFunction()
    };
    // console.log(message);
    if (message.author.bot || message.author.id !== config.id) return;
    if (!message.content?.startsWith(config.prefix)) return;

    const args = message.content.split(" ");
    const command = args.shift().substr(config.prefix.length);

    message.args = args;
    const cmd = commands.get(command);
    if (!cmd) return debug("unknown command");

    try {
      await cmd.run(message);
    } catch (error) {
      console.error(error);
    }
  });
};
