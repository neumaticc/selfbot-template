const { webhooks } = require("../config");
const Webhook = require("./types/Webhook");

module.exports = {
  alerts_ghostping: new Webhook(webhooks.ghostping),
  alerts_giveaways: new Webhook(webhooks.giveaways),
  alerts_friends: new Webhook(webhooks.friends),
};
