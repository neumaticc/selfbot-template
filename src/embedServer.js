/* eslint-disable consistent-return */
const fastify = require("fastify")({ logger: true });

const embeds = new Map();
const template = "<meta property=\"og:title\" content=\"[title]\">"
               + "\n<meta name=\"theme-color\" content=\"[color]\">"
               + "\n<meta property=\"og:description\" content=\"[description]\">";
//    + "<meta property=\"og:image\" content=\"[image]\">";
//    + "<link type=\"application/json+oembed\" href=\"https://amogus.loca.lt/api/embed/get/[id]/json\">";

fastify.get("/embed", async (request, reply) => {
  if (!request.headers["user-agent"].includes("Discordbot")) return reply.redirect("https://neumatic.club?ref=retarded");

  const embedData = embeds.get(request.query.id);
  if (!embedData) return reply.redirect("https://neumatic.club?ref=retarded2");

  const html = template
    .replace("[title]", embedData?.title)
    .replace("[color]", embedData?.color || "#FFAFCC")
    .replace("[description]", embedData?.description || "");

  reply.type("text/html").send(html);
});

module.exports.init = () => {
  fastify.listen({
    port: 5000,
  });
};

module.exports.embeds = embeds;
