const fs = require("fs");
// const debug = require("debug")("src:discordapp_events:PRESENCE_UPDATE");
// const { Alerts } = require("../types");
// const webhooks = require("../webhooks");
/* REL
{
  "user": {
    "username": "qoft",
    "public_flags": 0,
    "id": "141004982823550986",
    "discriminator": "3119",
    "avatar_decoration": null,
    "avatar": "1bbb883a8a3212ac97575e6a1f0411ed"
  },
  "type": 1,
  "nickname": null,
  "id": "141004982823550986",
  "exp": 1667954034087
}
*/

const run = async (data) => {
  // debug(JSON.stringify(data, null, 2));
  if (fs.existsSync(`./cache/relationships/${data.d.user.id}.json`)) {
    const relData = JSON.parse(fs.readFileSync(`./cache/relationships/${data.d.user.id}.json`, "utf8"));

    if (!relData.presence) relData.presence = data.d.status;
    if (!relData.customStatus) {
      const typeFour = data.d.activities.find((x) => x.type === 4);
      if (!typeFour) relData.customStatus = null;
      else relData.customStatus = typeFour;
    }

    // const relSimple = {
    //   userId: relData.user.id,
    // }
  }
};

module.exports = {
  name: "PRESENCE_UPDATE",
  run,
};
