const fs = require("fs");
// temporarily disabled: needs more research

// eslint-disable-next-line no-unused-vars
const run = async (data) => {
  if (!fs.existsSync("./cache/sessions.json")) return;
  const oldSessions = fs.readFileSync("./cache/sessions.json");

  let deletedSessions = oldSessions;
  let addedSessions = data.d;

  data.d.forEach((newSession) => {
    deletedSessions = deletedSessions.filter((x) => x.session_id !== newSession.session_id);
  });

  oldSessions.forEach((session) => {
    addedSessions = addedSessions.filter((x) => x.session_id !== session.session_id);
  });

  if (deletedSessions.length) {
    console.log("session deleted", deletedSessions, deletedSessions.toString());
  }

  if (addedSessions.length) {
    console.log("session added", addedSessions);
  }
};

module.exports = {
  name: "SESSIONS_REPLACE",
  run: () => {},
};
