const fs = require("fs");
const debug = require("debug")("event:MESSAGE_CREATE");
const config = require("../../config");
const { getChannel, getGuild } = require("../cache");
const detectGiveaway = require("../utilities/detectGiveaway");

const run = async (data) => {
  // fs.writeFileSync(`./cache/dev-messages/${data.d.id}.json`, JSON.stringify({
  //   ...data.d,
  // }, null, 2));

  // detect pings
  let pinged = false;
  if (data.d.mentions.length) {
    const atMe = data.d.mentions.find((x) => x.id === config.id);
    if (atMe) pinged = true;
  }
  if (data.d.mention_everyone) pinged = true;

  if (pinged) {
    debug("pinged", data.d.id);
    const channelData = await getChannel(data.d.channel_id);
    const guildData = data.d?.guild_id ? await getGuild(data.d.guild_id) : undefined;

    fs.writeFileSync(`./cache/messages/${data.d.id}.json`, JSON.stringify({
      ...data.d,
      channel: channelData,
      guild: guildData,
      exp: Date.now() + 60000 * 10, // 10m
    }, null, 2));
  }

  // detect and enter giveaways
  await detectGiveaway(data.d);
};

module.exports = {
  name: "MESSAGE_CREATE",
  run,
};
