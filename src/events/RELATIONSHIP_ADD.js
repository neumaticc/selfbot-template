/* eslint-disable no-case-declarations */
const fs = require("fs");
const debug = require("debug")("src:discordapp_events:RELATIONSHIP_ADD");
const { Alerts } = require("../types");
const webhooks = require("../webhooks");

const run = async (data) => {
  switch (data.d.type) {
    case 4: // outgoing request
      fs.writeFileSync(`./cache/relationships/${data.d.id}.json`, JSON.stringify(data.d, null, 2));

      const message = new Alerts.FriendAdded(data.d, false);
      await webhooks.alerts_friends.sendMessage(message);
      break;
    case 3: // incoming request
      fs.writeFileSync(`./cache/relationships/${data.d.id}.json`, JSON.stringify(data.d, null, 2));

      const message2 = new Alerts.FriendAdded(data.d, true);
      await webhooks.alerts_friends.sendMessage(message2);
      break;
    default:
      debug(`Relationship type ${data.d.type} not found`);
  }
};

module.exports = {
  name: "RELATIONSHIP_ADD",
  run,
};
