const fs = require("fs");
const debug = require("debug")("src:discordapp_events:RELATIONSHIP_ADD");
const { Alerts } = require("../types");
const webhooks = require("../webhooks");

const run = async (data) => {
  switch (data.d.type) {
    case 4: // outgoing request
      if (fs.existsSync(`./cache/relationships/${data.d.id}.json`)) {
        const outgoingRequestData = JSON.parse(fs.readFileSync(`./cache/relationships/${data.d.id}.json`, "utf8"));

        const message = new Alerts.FriendRemoved(outgoingRequestData, false);
        await webhooks.alerts_friends.sendMessage(message);
      }
      break;
    case 3: // incoming request
      if (fs.existsSync(`./cache/relationships/${data.d.id}.json`)) {
        const incomingRequestData = JSON.parse(fs.readFileSync(`./cache/relationships/${data.d.id}.json`, "utf8"));

        const message2 = new Alerts.FriendRemoved(incomingRequestData, true);
        await webhooks.alerts_friends.sendMessage(message2);
      }
      break;
    default:
      debug(`Relationship type ${data.d.type} not found`);
  }
};

module.exports = {
  name: "RELATIONSHIP_REMOVE",
  run,
};
