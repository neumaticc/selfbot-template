const fs = require("fs");
const debug = require("debug")("api_event:MESSAGE_DELETE");
const { Alerts } = require("../types");
const webhooks = require("../webhooks");

const run = async (data) => {
  debug(data);
  const { id } = data.d;
  if (fs.existsSync(`./cache/messages/${id}.json`)) {
    const parsed = JSON.parse(fs.readFileSync(`./cache/messages/${id}.json`));
    const message = new Alerts.Ghostping(parsed);
    await webhooks.alerts_ghostping.sendMessage(message);
  }
};

module.exports = {
  name: "MESSAGE_DELETE",
  run,
};
