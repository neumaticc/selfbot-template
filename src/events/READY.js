const fs = require("fs");

const run = async (data) => {
  fs.writeFileSync("./cache/ready.json", JSON.stringify(data.d, null, 2));

  // .user
  fs.writeFileSync("./cache/user.json", JSON.stringify({ ...data.d.user, session_id: data.d.session_id }, null, 2));

  // guilds
  fs.writeFileSync("./cache/guilds.json", JSON.stringify(data.d.guilds, null, 2)); // 48h TTL
  data.d.guilds.forEach((guild) => {
    fs.writeFileSync(`./cache/guilds/${guild.id}.json`, JSON.stringify({ ...guild, exp: Date.now() + 60000 * 60 * 48 }, null, 2)); // 48h TTL
  });

  // relationships
  data.d.relationships.forEach((r) => {
    fs.writeFileSync(`./cache/relationships/${r.id}.json`, JSON.stringify({ ...r, exp: Date.now() + 60000 * 60 * 48 }, null, 2)); // 48h TTL
  });

  // sessions
  fs.writeFileSync("./cache/sessions.json", JSON.stringify(data.d.sessions, null, 2));
};

module.exports = {
  name: "READY",
  run,
};
